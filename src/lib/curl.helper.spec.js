import { curlBuilder } from './curl.helper';


describe('Curl_Builder', () => {
  const mockRequest = {
    headers: {

    },
    method: 'GET',
    url: 'http://mockurl',
  };

  it('CurlHelper should build curl', async () => {
    expect(curlBuilder(mockRequest)).toBeDefined();
  });

  it('CurlHelper should handle if post data is object/ param is set/ should filer empty header/handle query params', async () => {
    expect(curlBuilder({
      ...mockRequest,
      ...{
        headers: {
          common: {},
          post: {
            'content-type': 'application/json',
          },
          xyz: 3,
          mno: 'abc',
          hij: {"a":1}
        },
        method: 'post',
        data: { a: 1 },
        params: { b: 2, c: 2 },
      },
    })).toBeDefined();
  });

  it('CurlHelper should handle if inline is enabled', async () => {
    expect(curlBuilder({
      ...mockRequest,
      ...{
        headers: {
          common: {},
          post: {
            'content-type': 'application/json',
          },
          xyz: 3,
          mno: 'abc'
        },
        method: 'post',
        data: 'sample',
        params: { b: 2 },
      },
    }, { inline: false })).toBeDefined();
  });
});
