export class CurlHelper {
  constructor(config) {
    this.request = config;
  }

  jsonStringify = (data) => {
    if(typeof data !== 'string' && typeof data !== 'number') {
      return JSON.stringify(data);
    } else{
      return data
    }
  }

  getHeaders() {
    let { headers } = this.request;
    // add any custom headers (defined upon calling methods like .get(), .post(), etc.)
    return Object.entries(headers)
      .filter(([property, propertyVal]) => ['common','delete','get','head', 'post', 'put', 'patch'].indexOf(property) === -1 )
      .map(([property, propertyVal]) => `${property}: ${this.jsonStringify(propertyVal)}`)
      .reduce((curlHeaders, currentHeader) => `${curlHeaders} 
    -H '${currentHeader}' `, '').trim();
  }

  getMethod() {
    this.request.method = this.request.method.toUpperCase()
    return `-X ${this.request.method.toUpperCase()}`;
  }

  getBody() {
    if ((typeof this.request.data !== 'undefined') && this.request.data !== '' && Object.keys(this.request.data).length && this.request.method.toUpperCase() !== 'GET') {
      const data = (typeof this.request.data === 'object' || Array.isArray(this.request.data)) ? JSON.stringify(this.request.data, null, 6) : this.request.data;
      return `-d '${data}'`;
    }
    return '';
  }

  getUrl() {
    return this.request.url.trim();
  }

  getParams() {
    return (this.request.params && Object.keys(this.request.params).length > 0) ? `${Object.entries(this.request.params)
      .map(([key, val]) => `${key}=${val}`)
      .reduce((all, current) => `${!all.endsWith('?') ? `${all}&` : `${all}`}${current}`, '?')}` : '';
  }

  generateCommand(inline) {
    let command = `
    curl ${this.getMethod()} 
   '${this.getUrl()}${this.getParams()}'
    ${this.getHeaders()}
    ${this.getBody()}`.trim()
      .replace(/\\/g, '\\\\');

    if (inline) {
      command = command
        .replace(/\s{2,}/g, ' ')
        .replace(/\n/g, '');
    }
    return command;
  }
}

export const curlBuilder = (config, { inline = true } = {}) => {
  const curlHelper = (new CurlHelper(config));
  return curlHelper.generateCommand(inline);
};
