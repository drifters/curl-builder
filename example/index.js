const { curlBuilder } = require('../dist');

console.log(curlBuilder({
    method: 'GET',
    url: 'http://mockurl',
    headers: {
        xyz: 3,
        mno: "abc"
    },
    data: { a: 1 },
    params: { b: 2, "c[]": 2 },
}))