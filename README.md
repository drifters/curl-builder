# @drifters/curl-builder

> Curl builder used to dynamically building curls

[![Generic badge](https://img.shields.io/badge/Bitbucket-@drifters/curl--builder-green?logo=bitbucket&style=flat&labelColor=0052CC&color=6C2E84&link=http://left&link=http://right)](https://bitbucket.org/bdrifter/drifters-library/src/master/packages/utils/curl-builder/)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
[![Babel](https://img.shields.io/badge/Babel-7.9.0-green?logo=Babel&style=flat&color=F9DC3E&link=http://left&link=http://right)](https://bitbucket.org/bdrifter/drifters-library/src/master/)
[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)

## Install

```bash
npm i --save @drifters/curl-builder
```
## Example

```
const { curlBuilder } = require('@drifters/curl-builder');

console.log(curlBuilder({
    method: 'GET',
    url: 'http://mockurl',
    headers: {
        xyz: 3,
        mno: "abc"
    },
    data: { a: 1 },
    params: { b: 2, "c[]": 2 },
}))
```
## License

MIT © [suminksudhi](https://github.com/suminksudhi)
